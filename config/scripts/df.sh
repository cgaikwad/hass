#!/bin/bash
FS=$2
SERVER=$1
ssh -o StrictHostKeyChecking=no -i /config/keys/id_rsa -l chris $SERVER df $FS --output=pcent | tail -1 | sed 's/ \(.*\)%/\1/g'
